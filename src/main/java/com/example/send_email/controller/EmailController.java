package com.example.send_email.controller;

import com.example.send_email.resource.EmailMessage;
import com.example.send_email.service.EmailSenderService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.lang.reflect.Modifier;

@RestController
public class EmailController {
    private final EmailSenderService emailSenderService;

    public EmailController(EmailSenderService emailSenderService) {
        this.emailSenderService = emailSenderService;
    }

    @PostMapping("/send-email")
    public ResponseEntity sendEmail(@RequestBody EmailMessage emailMessage){
        this.emailSenderService.sendEmail(emailMessage);
        return ResponseEntity.ok("Success");
    }
    @GetMapping("/index")
    public ModelAndView form(){
        EmailMessage message=new EmailMessage();
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.setViewName("email_form");
        modelAndView.addObject("email",message);
        return modelAndView;
    }
    @PostMapping("/send-email-form") public ModelAndView sendEmailTh(@ModelAttribute("email") EmailMessage emailMessage){
        emailSenderService.sendEmail(emailMessage);
      ModelAndView modelAndView=new ModelAndView();
     modelAndView.setViewName("redirect:/index");
        return modelAndView;
    }
}
