package com.example.send_email.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmailMessage {
    private String sentTo;
    private String senderName;
    private String subject;
    private String message;
}
