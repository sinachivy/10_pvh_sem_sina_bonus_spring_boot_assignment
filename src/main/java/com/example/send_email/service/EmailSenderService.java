package com.example.send_email.service;

import com.example.send_email.resource.EmailMessage;

public interface EmailSenderService {
    String sendEmail(EmailMessage emailMessage);
}
