package com.example.send_email.service.serviceImp;

import com.example.send_email.resource.EmailMessage;
import com.example.send_email.service.EmailSenderService;
import jakarta.mail.internet.MimeMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderServiceImp implements EmailSenderService {
    private final JavaMailSender mailSender;

    public EmailSenderServiceImp(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public String sendEmail(EmailMessage emailMessage) {

//        SimpleMailMessage simpleMailMessage=new SimpleMailMessage();
//        simpleMailMessage.setFrom("sinachivy@gmail.com");
//        simpleMailMessage.setTo(sendTo);
//        simpleMailMessage.setSubject(subject);
//        simpleMailMessage.setText(message);
//        mailSender.send(simpleMailMessage);
        try{

            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper= new MimeMessageHelper(mimeMessage);
            mimeMessageHelper.setSubject("Welcome " + emailMessage.getSubject());
            String html = "<!doctype html>\n" +
                    "<html lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\"\n" +
                    "      xmlns:th=\"http://www.thymeleaf.org\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <meta name=\"viewport\"\n" +
                    "          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\n" +
                    "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n" +
                    "    <title>Email</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div> Your username is <b>" + emailMessage.getSenderName() + "</b></div>\n" +
                    "<div> <h1>" + emailMessage.getSubject() + "</h1></div>\n" +
                    "\n" +
                    "<div> <p>" + emailMessage.getMessage()+ "</p></div>\n" +
                    "\n" +

                    "</body>\n" +
                    "</html>\n";
// Sending the mail
            mimeMessageHelper.setText(html, true);
            mimeMessageHelper.setTo(emailMessage.getSentTo());
            mailSender.send(mimeMessage);
            return "Mail Sent Successfully...";

        }catch (Exception e){
         return "Ot tv";
        }


    }
}
